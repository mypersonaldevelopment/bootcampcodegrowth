// Title        : String extraction 
// Purpose      : Implement and test String extraction in Javascript

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 202003       SHPRLK              Created the file and ran the basic extraction test
//----------------------------------------------------------------------------------------------------------------------

function StringExtraction ()
{
    var message = "Hello, This is a big message and i am supposed to limit it to a certain number of characters";
    var messageLength    = message.length;
    console.log(" the accepted size message is : " + message.slice(0,71));    
}

StringExtraction();
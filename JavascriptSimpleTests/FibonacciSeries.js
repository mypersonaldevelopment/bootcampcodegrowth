// Title        : Fibonacci series  
// Purpose      : Implement the Fibonacci series with for loop

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200314       SHPRLK              Created the file, implemented and tested the Fibonacci series
//----------------------------------------------------------------------------------------------------------------------


function FibonacciSeries(numberOfElements_){  
    var result_ = [];  
    if(numberOfElements_> 0){
       if(numberOfElements_==1){
           result_.push(0);
       }
       else 
       {
           result_ = [0,1];
           for(var element_ = 2; element_ < numberOfElements_ ; element_++){               
               result_.push( (result_[element_-1] ) + (result_[element_ -2]) );               
           }
       }
       return result_;
    }
    else 
    {
        return 0;
    }    
}


console.log(FibonacciSeries(15));














// function FibonacciSeries(numberOfElements_){
//     if(numberOfElements_> 0){
//         result_ = [0];
//         for(var elementCount_ = 2 ; elementCount_ <= numberOfElements_ ; elementCount_++){
//             if (numberOfElements_ === 1 ){
//                 result_.push(1);
//             }
//             else{
//                 console.log(' result_[(result_.length-2)  : '+ result_[(result_.length-2));
//                 console.log(' result_[(result_.length-1)  : '+ result_[(result_.length-1));
//                 result_.push((result_[(result_.length-2)] + result_[(result_.length-1)]));
//             }    
//         }
//     }
//     else 
//     {
//         return 0;
//     }
// }

// FibonacciSeries(2);
// console.log(result_);
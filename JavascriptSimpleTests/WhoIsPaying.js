// Title        : Who is paying
// Purpose      : Implement and test a random selector to choose from a list of names
//                to pay the lunch bill  

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200313      SHPRLK              Created the file and implemented who is paying
//----------------------------------------------------------------------------------------------------------------------

var nameList_ = ["Abraham", "Lincoln", "Karl", "Benz", "Mercedez", "Bavarian"];

function WhoIsPaying(names_){
    var payeeIndex_ = (Math.floor((names_.length)*(Math.random())));
    console.log(names_[payeeIndex_] + " is going to buy lunch today!");
}

WhoIsPaying(nameList_);
// Title        : Javascript function for BMI Calculation 
// Purpose      : To Implement and test for calculating lifeteim in weeks from given age in years

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200304     SHPRLK              Created the file and ran BMI calculation
//----------------------------------------------------------------------------------------------------------------------

function BmiCalculator(weight_, height_){
    return (weight_/ Math.pow(height_,2));
}

var Bmi_ = BmiCalculator(69.0, 1.8);
console.log('Your BMI : ' + Bmi_);
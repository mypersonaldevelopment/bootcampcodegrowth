// Title        : Javascript function for Random number generator and Love calculator 
// Purpose      : To Implement and test for calculating lifeteim in weeks from given age in years

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200304     SHPRLK              Created the file and ran random number generator and love calculation
//----------------------------------------------------------------------------------------------------------------------

function RandomNumberGenerator(){
    var randomNumber = Math.random();
    return randomNumber;
}

var randomNoReturn = RandomNumberGenerator();
console.log(randomNoReturn);
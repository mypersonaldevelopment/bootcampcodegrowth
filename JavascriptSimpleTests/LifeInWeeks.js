// Title        : Javascript function for lifetime calculation in weeks 
// Purpose      : To Implement and test for calculating lifeteim in weeks from given age in years

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200303     SHPRLK              Created the file.
// 20200304     SHPRLK              Added the code for the calculation and console log.
//----------------------------------------------------------------------------------------------------------------------

function LifeInWeeks(age_){
    var remainingYears_ = 90 - age_;
    var weeks_   = remainingYears_*52;     
    var days_    = weeks_ * 7;
    var hours_   = days_ * 24;
    var minutes_ = hours_ * 60 ;
    var seconds_ = minutes_* 60;
    console.log( ' You have ' + weeks_ + ' weeks '+ days_ + ' days ' + hours_ + ' hours '+ minutes_+' minutes '+ seconds_ + ' seconds remaining in your life');
}

LifeInWeeks(30);


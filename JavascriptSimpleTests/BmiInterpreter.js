// Title        : Javascript function for if else conditions 
// Purpose      : To Implement and test conditional statements by interpreting BMI

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200304     SHPRLK              Created the file and ran BMI interpretation.
//----------------------------------------------------------------------------------------------------------------------

function BmiInterpretation(weight_, height_){
    var bmi_ = weight_/ (Math.pow(height_, 2));
    if (bmi_ < 18.5 ){
        console.log('Your BMI is : ' + bmi_+ ', so you are underweight');
    }
    else if (bmi_ < 24.9  ){
        console.log('Your BMI is : ' + bmi_+ ', so you have a normal weight');
    }
    else {
        console.log('Your BMI is : ' + bmi_+ ', so you are overwieght');
    }
}

BmiInterpretation(68,1.78);
// Title        : Simple Javascript swap 
// Purpose      : To Implement and test swapping of two variables

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200303     SHPRLK              Created the file and ran the basic swap test
//----------------------------------------------------------------------------------------------------------------------


function VariableSwap ()
{
    var a = "7";
    var b = "9";
    var swapVariable;                       //Following JS naming standard
    
    console.log('a initially is : ' + a);
    console.log('b initially is : ' + b);

    swapVariable = a; 
    a = b; 
    b = swapVariable;

    console.log('a is : ' + a);
    console.log('b is : ' + b);

}

VariableSwap();
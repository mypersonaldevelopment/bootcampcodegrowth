// Title        : 99 Bottles of beer song 
// Purpose      : Implement the song with use of while loop

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200314       SHPRLK              Created the file and ran the basic extraction test
//----------------------------------------------------------------------------------------------------------------------

function NintyNineBottlesOfBeer(){
    var numbeOfBottles_ = 99;
    while( numbeOfBottles_ > 0){
        console.log(numbeOfBottles_ + " bottles of beer on the wall, "+ numbeOfBottles_ + " bottles of beer");
        console.log("Take one down and pass it around, " + (numbeOfBottles_ - 1)+ " of beer on the wall");
        console.log();
        numbeOfBottles_--;
    }
    console.log("No more bottles of beer on the wall, No more bottles of beer");
    console.log("Go to the store and buy some more, 99 bottles of beer on the wall");
}

NintyNineBottlesOfBeer();
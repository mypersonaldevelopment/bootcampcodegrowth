// Title        : Javascript function IsLeapYear
// Purpose      : To Implement and test if a given year is a leap year

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200309     SHPRLK              Created the file and ran Leap year tests.
//----------------------------------------------------------------------------------------------------------------------

function IsLeapYear(year_){
    console.log('-----      -----    year '+ year_ + ' is : ');
    if (year_ % 100 == 0){
        if(year_ % 400 == 0){
           PrintLeapYear();            
        }
        else{
           PrintNotLeapYear();
        }        
    }
    else
    {
        if(year_ % 4 == 0){
            PrintLeapYear();
        }
        else{
            PrintNotLeapYear();
        }        
    }
}

function PrintLeapYear(){
    console.log('Leap year.');
}

function PrintNotLeapYear(){
    console.log('Not leap year.');
}

//-----------------Testing years--------------------------
IsLeapYear(2100);
IsLeapYear(2400);
IsLeapYear(2101);
IsLeapYear(2104);
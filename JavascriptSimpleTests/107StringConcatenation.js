// Title        : String Concatenation 
// Purpose      : Implement and test String concatenation in Javascript

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200303      SHPRLK              Created the file and ran the basic swap test
//----------------------------------------------------------------------------------------------------------------------

function StringConcatenation ()
{
    var message = "Hello";
    var name    = "Javascript";
    console.log(message + " there , welcome to the world of " + name);    
}

StringConcatenation();
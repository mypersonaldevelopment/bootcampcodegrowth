// Title        : Javascript function for Arrays  
// Purpose      : To Implement and FuzzBizzGame with arrays 

// DATE         CONTRIBUTOR ID      COMMENT
//---------     --------------      ------------------------------------------------------------------------------------
// 20200312     SHPRLK              Created the file and implemented array game with errors FuzzBizz
// 20200313     SHPRLK              Solved For loop declaration order to solve inifite loop error
//----------------------------------------------------------------------------------------------------------------------

var output_ = [];

function FuzzBizz(limit_){    
    for(var counter_ = 1; counter_ <= limit_; counter_++  ){
        if(counter_ % 15 === 0){
            output_.push('FuzzBizz');
        }
        else if(counter_ % 5 === 0){
            output_.push('Bizz');
        }
        else if(counter_ % 3 === 0){
            output_.push('Fuzz');
        }
        else{
            output_.push(counter_);
        }
        
    }
}

function FuzzBizzResult(limit_){
    FuzzBizz(limit_);
    output_.forEach(PrintValue);
}

function PrintValue(value_){
    console.log(value_);
}


FuzzBizzResult(100);